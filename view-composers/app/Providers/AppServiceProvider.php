<?php

namespace App\Providers;

use App\Channel;
use App\Http\View\Composers\ChannelsComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        Option one, not a good one
//        View::share('channels', Channel::orderBy('name')->get())



        // Option two, Granular view with wildcard
//        View::composer(['channel.index','post.create'], function($view)
//        {
//            $view->with('channels', Channel::orderBy('name')->get());
//        });



        // Option Three, Dedicated Class
        //View::composer(['channel.index','post.create'], ChannelsComposer::class);
        View::composer('partials.channels.*', ChannelsComposer::class);
    }
}
