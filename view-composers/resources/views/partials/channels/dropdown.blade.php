<select name="{{ $field ?? 'channel_id' }}" id="{{ $field ?? 'channel_id' }}">
    @forelse($channels as $channel)
        <option value="{{ $channel->id }}">{{ $channel->name }}</option>
    @empty
        <option hidden >No Channel Available</option>
    @endforelse
</select>
